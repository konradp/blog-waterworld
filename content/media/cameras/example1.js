class Ship {
  constructor() {
    console.log('constructor');
    this.speed = 0;
  };
};
const sh = new Ship();
console.log('sh.speed', sh.speed);
let v3 = (x, y, z) => new BABYLON.Vector3(x,y,z);
let v2 = (x, y) => new BABYLON.Vector2(x,y);
let q4 = BABYLON.Quaternion;

window.addEventListener('load', function() {
  const canvas = document.getElementById("example1");
  const engine = new BABYLON.Engine(canvas, true);

  // Do not scroll window on mouse wheel on keyboard
  canvas.addEventListener('wheel', e => e.preventDefault());
  canvas.addEventListener('keydown', function (e) {
    if ([
      'Space', 'ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'
    ].indexOf(e.code) > -1
    ) {
      e.preventDefault();
    }
  });

  const createScene = function() {
    // Create scene
    const scene = new BABYLON.Scene(engine);
    // House
    const house = BABYLON.MeshBuilder.CreateBox("box", { width: 1, height: 1, wrap: true, });
    house.position = new BABYLON.Vector3(2, 0.5, 1);
    const roof = BABYLON.MeshBuilder.CreateCylinder("roof", {
      diameter: 1.5,
      height: 1.2,
      tessellation: 3,
    });
    roof.position = new BABYLON.Vector3(2, 1.20, 1);
    roof.scaling.x = 0.60;
    roof.rotation.z = Math.PI/2;
    const roofMat = new BABYLON.StandardMaterial("roofMat");
    roofMat.diffuseColor = new BABYLON.Color3(0.7, 0.3, 0.3);
    roof.material = roofMat;

    // Ground
    const ground = BABYLON.MeshBuilder.CreateGroundFromHeightMap("largeGround", "https://assets.babylonjs.com/environments/villageheightmap.png", {width:150, height:150, subdivisions: 20, minHeight:0, maxHeight: 10});
    const groundMat = new BABYLON.StandardMaterial("largeGroundMat");
    groundMat.diffuseTexture = new BABYLON.Texture("https://assets.babylonjs.com/environments/valleygrass.png");
    ground.material = groundMat;

    // Light
    const light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(1, 1, 0));

    // Skybox
    const skybox = BABYLON.MeshBuilder.CreateBox("skyBox", {size:1000.0});
    const skyboxMaterial = new BABYLON.StandardMaterial("skyBox");
    skyboxMaterial.backFaceCulling = false;
    skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("/blog-waterworld/media/cameras/textures/skybox");
    skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
    skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
    skybox.material = skyboxMaterial;


    // Camera
    const rad = 10;
    const camera = new BABYLON.FlyCamera('camera',
      new BABYLON.Vector3(1, 2, -10), // position
    );
    camera.speed = 0.001;
    camera.attachControl(canvas, true);
    // Use quaternions to avoid gimbal lock
    //camera.rotationQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(
    //  camera.rotation.y, camera.rotation.x, camera.rotation.z);
    camera.rollCorrect = 0; // This is the ke

    return scene;
  };

  const scene = createScene();
  engine.runRenderLoop(function () {
    scene.render();
  });

  function moveShip() {
    // Call periodicaly. Move ship if ship speed is more than zero
    if (sh.speed == 0) return;
    console.log('Moving, speed:', sh.speed);
    const camera = scene.cameras[0];
    let dir = camera.target.subtract(camera.position);
    camera.spinTo('position', camera.position.add(dir.scale(sh.speed)), 150);
  }

  setInterval(moveShip, 500);

  BABYLON.FlyCamera.prototype.spinTo = function (whichprop, targetval, speed) {
    var ease = new BABYLON.CubicEase();
    ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
    BABYLON.Animation.CreateAndStartAnimation('at4', this, whichprop, speed, 120, this[whichprop], targetval, 0, ease);
  }

  BABYLON.FlyCamera.prototype.rotateTo = function (whichprop, targetval, speed) {
    var ease = new BABYLON.CubicEase();
    ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
    BABYLON.Animation.CreateAndStartAnimation('at3', this, 'rotation.z', speed, 120, this.rotation.z, targetval, 0, ease);
  }

  // Mouse
  scene.onPointerObservable.add((e) => {
    if (e.type == BABYLON.PointerEventTypes.POINTERWHEEL) {
      if (e.event.deltaY == -120 && sh.speed < 10) {
        // Increase speed
        sh.speed += 1;
        console.log('speed++');
      } else if (e.event.deltaY == 120 && sh.speed > 0) {
        // Decrease speed
        sh.speed -= 1;
        console.log('speed--');
      }
    } else {
      // Mouse event
      console.log(e);
    }
  });

  // Keyboard
  scene.onKeyboardObservable.add((e) => {
    const camera = scene.cameras[0];
    switch (e.type) {
      case BABYLON.KeyboardEventTypes.KEYDOWN:
        const up = camera.upVector;
        const target = camera.target;
        const pitchAxis = up.cross(target);

        const currentRotation = BABYLON.Quaternion.RotationYawPitchRoll(camera.rotation.y, camera.rotation.x, camera.rotation.z);
        let rotationChange = null;
        let rot = 0.1;
        switch (e.event.key) {
          case 'w': // down FIX
            rotationChange = q4.RotationAxis(BABYLON.Axis.X, rot);
            break;
          case 'a': // roll left
            rotationChange = q4.RotationAxis(BABYLON.Axis.Z, rot);
            break;
          case 's': // up
            rotationChange = q4.RotationAxis(BABYLON.Axis.X, -rot);
            break;
          case 'd': // roll right
            rotationChange = q4.RotationAxis(BABYLON.Axis.Z, -rot);
            break;
        }
        currentRotation.multiplyInPlace(rotationChange);
        currentRotation.toEulerAnglesToRef(camera.rotation);
        break;
      case BABYLON.KeyboardEventTypes.KEYUP:
        //console.log("KEY UP: ", e.event.code);
        break;
    }
  }); // end onKeyboardObservable
});
