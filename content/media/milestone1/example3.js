window.addEventListener('load', function() {
  const canvas = document.getElementById("example3"); // Get the canvas element
  //canvas.addEventListener('wheel', e => e.preventDefault());

  const engine = new BABYLON.Engine(canvas, true); // Generate the BABYLON 3D engine
  const createScene = function() {
    const PI = Math.PI;
    const scene = new BABYLON.Scene(engine);

    // Ship
    const ship = BABYLON.MeshBuilder.CreateCylinder("triangle", {
      diameter: 3,
      tessellation: 3,
      height: 0.3,
      faceColors: [
        new BABYLON.Color4(1, 0, 0, 1),
        new BABYLON.Color4(0, 1, 0, 1),
        new BABYLON.Color4(0, 0, 1, 1),
      ],
    });
    ship.scaling.x = 1.4;
    //ship.rotation = new BABYLON.Vector3(0, 1*PI/6, 0);

    // Camera and light
    const rad = 15;
    const camera = new BABYLON.ArcRotateCamera("camera",
      -Math.PI / 2,
      Math.PI / 2,
      rad,
      new BABYLON.Vector3(0, 0, 0)
    );
    camera.lowerRadiusLimit = rad;
    camera.upperRadiusLimit = rad;
    camera.attachControl(canvas, true);
    const light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(1, 1, 0));

    // Animations
    ship.animations = [];
    // Animation: X
    const animX = new BABYLON.Animation(
      "animX",
      "position.x",
      30,
      BABYLON.Animation.ANIMATIONTYPE_FLOAT,
      BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE)
    ;
    const lRad = 6;
    const xKeys = [
      { frame: 0,    value: lRad },
      { frame: 12.5, value: 10 },
      { frame: 25,   value: lRad },
      { frame: 75,   value: -lRad },
      { frame: 87.5, value:-10 },
      { frame: 100,  value: -lRad },
      { frame: 150,  value: lRad },
    ]; 
    animX.setKeys(xKeys);
    ship.animations.push(animX);
    // Animation: Y
    const animY = new BABYLON.Animation(
      "animY",
      "position.y",
      30,
      BABYLON.Animation.ANIMATIONTYPE_FLOAT,
      BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE)
    ;
    const yKeys = [
      { frame: 0,   value:-5 },
      { frame: 25,  value: 5 },
      { frame: 50,  value: 0 },
      { frame: 75,  value:-5 },
      { frame: 100, value: 5 },
      { frame: 125, value: 0 },
      { frame: 150, value:-5 },
    ]; 
    animY.setKeys(yKeys);
    ship.animations.push(animY);

    // Animation: Pitch
    const animPitch = new BABYLON.Animation(
      "animY",
      "rotation.z",
      30,
      BABYLON.Animation.ANIMATIONTYPE_FLOAT,
      BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE)
    ;
    const pitchKeys = [
      { frame: 0,   value:0 },
      { frame: 12.5,   value:PI/2 },
      { frame: 25,   value:PI },
      { frame: 50,   value:PI+PI/4 },
      { frame: 75,   value:PI },
      { frame: 87.5,   value:PI/2 },
      { frame: 100,   value:0 },
      { frame: 125,   value:-PI/4 },
      { frame: 150, value:0 },
    ]; 
    animPitch.setKeys(pitchKeys);
    ship.animations.push(animPitch);

    // Animations: enable
    scene.beginAnimation(ship, 0, 150, true);

    // Draw eight lines
    let R = lRad;
    const myPoints = [
      new BABYLON.Vector3(   R, -5, 0),
      new BABYLON.Vector3(  10,  0, 0),
      new BABYLON.Vector3(   R,  5, 0),
      new BABYLON.Vector3(   0,  0, 0),
      new BABYLON.Vector3(  -R, -5, 0),
      new BABYLON.Vector3( -10,  0, 0),
      new BABYLON.Vector3(  -R,  5, 0),
      new BABYLON.Vector3(   0,  0, 0),
      new BABYLON.Vector3(   R, -5, 0),
    ]
    const lines = BABYLON.MeshBuilder.CreateLines("lines", {points: myPoints});

    return scene;
  }; // end createScene()
  const scene = createScene();
  engine.runRenderLoop(function () {
    scene.render();
  });
});

window.addEventListener("resize", function () {
  engine.resize();
});
