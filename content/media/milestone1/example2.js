window.addEventListener('load', function() {
  const canvas = document.getElementById("example2"); // Get the canvas element
  canvas.addEventListener('wheel', e => e.preventDefault());

  const engine = new BABYLON.Engine(canvas, true); // Generate the BABYLON 3D engine
  const createScene = function() {
    const PI = Math.PI;
    const scene = new BABYLON.Scene(engine);

    // Ship
    const ship = BABYLON.MeshBuilder.CreateCylinder("triangle", {
      diameter: 3,
      tessellation: 3,
      height: 0.3,
      faceColors: [
        new BABYLON.Color4(1, 0, 0, 1),
        new BABYLON.Color4(0, 1, 0, 1),
        new BABYLON.Color4(0, 0, 1, 1),
      ],
    });
    ship.scaling.x = 1.4;
    ship.rotation = new BABYLON.Vector3(0, 1*PI/6, 0);

    // Camera and light
    const rad = 4;
    const camera = new BABYLON.ArcRotateCamera("camera",
      -Math.PI / 2,
      Math.PI / 2.5,
      rad,
      new BABYLON.Vector3(0, 0, 0)
    );
    camera.lowerRadiusLimit = rad;
    camera.upperRadiusLimit = rad;
    camera.attachControl(canvas, true);
    const light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(1, 1, 0));
    return scene;
  }; // end createScene()
  const scene = createScene();
  engine.runRenderLoop(function () {
    scene.render();
  });
});

window.addEventListener("resize", function () {
  engine.resize();
});
