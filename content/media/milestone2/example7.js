window.addEventListener('load', function() {
  const canvas = document.getElementById("example7"); // Get the canvas element
  canvas.addEventListener('wheel', e => e.preventDefault());
  canvas.addEventListener('keydown', (e) => {
    if (['Space', 'ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'].indexOf(e.code) > -1) {
      e.preventDefault();
    }
  });


  const engine = new BABYLON.Engine(canvas, true); // Generate the BABYLON 3D engine
  const createScene = function() {
    const PI = Math.PI;
    const scene = new BABYLON.Scene(engine);

    // Ship
    const ship = BABYLON.MeshBuilder.CreateCylinder("triangle", {
      diameter: 3,
      tessellation: 3,
      height: 0.3,
      faceColors: [
        new BABYLON.Color4(1, 0, 0, 1),
        new BABYLON.Color4(0, 1, 0, 1),
        new BABYLON.Color4(0, 0, 1, 1),
      ],
    });
    ship.scaling.x = 1.4;
    ship.rotation = new BABYLON.Vector3(0, PI/2, 0);

    // Ship axis of rotation
    var pivot = new BABYLON.TransformNode('root');
    ship.parent = pivot;
    pivot.animations = [];

    // Camera and light
    const rad = 18;
    const camera = new BABYLON.FreeCamera("camera", new BABYLON.Vector3(0, 5, -10));
    //camera.lowerRadiusLimit = rad;
    //camera.upperRadiusLimit = rad;
    camera.attachControl(canvas, true);
    const light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(1, 1, 0));


    // Cubic bezier path
    const R = 6;
    let v3 = (x, y, z) => new BABYLON.Vector3(x,y,z);
    let curve = BABYLON.Curve3.CreateCubicBezier(
      v3(R , -5, 0),
      v3(15, -R, 0),
      v3(15,  R, 0),
      v3(R ,  5, 0),
      20
    );
    let curveCont = BABYLON.Curve3.CreateCubicBezier(
      v3(R, 5, 0),
      v3(-R/2, R/2, 0),
      v3(R/2, -R/2, 0),
      v3(-R, -5, 0),
      20
    );
    curve = curve.continue(curveCont);
    curveCont = BABYLON.Curve3.CreateCubicBezier(
      v3(-R , -5, 0),
      v3(-15, -R, 0),
      v3(-15,  R, 0),
      v3(-R ,  5, 0),
      20
    );
    curve = curve.continue(curveCont);
    curveCont = BABYLON.Curve3.CreateCubicBezier(
      v3(-R, 5, 0),
      v3(R/2, R/2, 0),
      v3(-R/2, -R/2, 0),
      v3(R, -5, 0),
      20
    );
    curve = curve.continue(curveCont);
    var curveMesh = BABYLON.MeshBuilder.CreateLines(
      "bezier", {points: curve.getPoints()}, scene);
    curveMesh.color = new BABYLON.Color3(1, 1, 0.5);


    // Transform the curves into a proper Path3D object and get its orientation information
    var path3d = new BABYLON.Path3D(curve.getPoints());
    var tangents = path3d.getTangents();
    var normals = path3d.getNormals();
    var binormals = path3d.getBinormals();
    var curvePath = path3d.getCurve();

    const frameRate = 60;
    const posAnim = new BABYLON.Animation("cameraPos", "position",
      frameRate, BABYLON.Animation.ANIMATIONTYPE_VECTOR3);
    const posKeys = [];
    const rotAnim = new BABYLON.Animation("cameraRot", "rotationQuaternion",
      frameRate, BABYLON.Animation.ANIMATIONTYPE_QUATERNION);
    const rotKeys = [];
    for (let i = 0; i < curvePath.length; i++) {
      const position = curvePath[i];
      const tangent = tangents[i];
      const binormal = binormals[i];
      const rotation = BABYLON.Quaternion.FromLookDirectionLH(tangent, binormal);
      posKeys.push({frame: i * frameRate, value: position});
      rotKeys.push({frame: i * frameRate, value: rotation});
    }
    posAnim.setKeys(posKeys);
    rotAnim.setKeys(rotKeys);
    pivot.animations.push(posAnim);
    pivot.animations.push(rotAnim);

    // Animation: Roll
    const dur = frameRate*curvePath.length;
    const animRoll = new BABYLON.Animation(
      "animRoll",
      "rotation.x",
      frameRate,
      BABYLON.Animation.ANIMATIONTYPE_FLOAT,
      BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE
    );
    const rollKeys = [
      { frame:  (0/10)*dur, value: 0    },
      { frame:  (3/10)*dur, value: 0    }, // roll 1 start
      { frame:  (4/10)*dur, value: PI   }, // roll 1 end
      { frame:  (8/10)*dur, value: PI   }, // roll 2 start
      { frame:  (9/10)*dur, value: 2*PI }, // roll 2 end
      { frame: (curvePath.length-1)*frameRate, value: 2*PI },
    ]; 
    animRoll.setKeys(rollKeys);
    ship.animations.push(animRoll);

    // Animations: enable
    scene.animationTimeScale = 10;
    scene.beginAnimation(pivot, 0, frameRate*curvePath.length, true);
    scene.beginAnimation(ship, 0, dur, true);

    return scene;
  }; // end createScene()
  const scene = createScene();
  engine.runRenderLoop(function () {
    scene.render();
  });
});

window.addEventListener("resize", function () {
  engine.resize();
});
