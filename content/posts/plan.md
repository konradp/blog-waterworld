---
title: "Plan"
subtitle: 
date: 2023-04-16
tags: [ "wip" ]
---

{{<toc>}}

## Milestones
- **Milestone 1: First steps in 3D**
  - {{<v>}} study BabylonJS
  - {{<v>}} draw ship model: triangle
  - {{<v>}} animate ship: just flying around, no need to worry about physics
- **Milestone 2: FPS camera and controls**
  - {{<v>}} cockpit camera: first-person view
  - {{<x>}} basic ship steering: yaw/pitch/roll
  - {{<x>}} draw cockpit overlay?
- **Milestone 3:**
  - {{<x>}} think about physics of swimming the ship
  - {{<x>}} decide controls: e.g. engine thrust with mouse wheel
- **Milestone 4:**
  - {{<x>}} ship shooting projectiles/rockets
  - {{<x>}} multiplayer
  - {{<x>}} BIG: loading meshes dynamically based on the closeness
  - {{<x>}} atmosphere: fog
  - {{<x>}} improve rockets/projectiles: maybe two types: directional or target-locking
  - {{<x>}} design ships: study current submarine vehicles
- **Other/major:**
  - {{<v>}} create a separate blog for this game
- **Other/minor:**
  - {{<x>}} VR?
  - {{<x>}} looking around the cockpit? e.g. with mouse
  - {{<x>}} gamepad?
  - {{<x>}} joystick or other flight-specific controls? Do study here on how subma

## General ideas

- happens under water, but is a lot like a space game, with ships, warships etc
- TODO: Study physics of underwater:
  - pressure: swimming at significant depths involves significant amount of pressure

## Gameplay
- cockpit view
    - also a console, like in quake?
- base view: text-based, like MUDs

## Gameplay/stories
- docking autopilot:  
  without pilot's licence, once close enough to a base ship (base zone), an autopilot will kick in to either dock the ship or more out of the base zone
- pilot's licence:  
  allow manual control near the base ship
- study the underwater vehicles
  - UUV: unmanned
  - ROV: remotely operated
  - https://en.wikipedia.org/wiki/Wet_sub
