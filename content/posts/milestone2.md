---
title: "Milestone 2: FPS camera and controls"
subtitle: 
date: 2023-04-16
tags: [ "wip" ]
---
<script src="https://cdn.babylonjs.com/babylon.js"></script>
<script src="/media/milestone2/example7.js"></script>
<script src="/media/milestone2/example8.js"></script>
<style>
  canvas { width: 70%; },
  img { width: 70%; },
</style>

{{<toc>}}

Goals
- cockpit camera: first-person view
- basic ship steering: yaw/pitch/roll
- draw cockpit overlay?

For the first goal, we change from the ArcRotateCamera to Universal Camera (FreeCamera).

<center><canvas id='example7'></canvas></center>

## Ship steering, customising controls: yaw/pitch/roll
For the second goal, I read the documentation:
- [BabylonJS: Customizing camera inputs](https://doc.babylonjs.com/features/featuresDeepDive/cameras/customizingCameraInputs)

I would like controls based on WASD+JKL. Ideally, I would like the controls to take no effect if the ship is stationary, i.e. the ship should only turn if the engine is pushing the ship forward:
- yaw (turn left/right): J + L
- pitch (up/down): S + W
- roll: (roll left/right): A + D
- engine up/down: I + J, or scroll wheel (including mouse pad scroll)

But, reading through the [API documentation](https://doc.babylonjs.com/typedoc/classes/BABYLON.FlyCamera), I found that there is also a FlyCamera, but once look straight down, the camera starts spinning, and you cannot roll it upside down.

So I take a detour to study the cameras, a separate [blog post here](/post/com-babylonjs-cameras/)

<center><canvas id='example8'></canvas></center>
