---
title: "Cameras"
subtitle: 
date: 2023-04-23
tags: [ "wip" ]
---

<script src="https://cdn.babylonjs.com/babylon.js"></script>
<script src="/media/cameras/example1.js"></script>
<style>
  canvas { width: 70%; },
  img { width: 70%; },
</style>


## Cameras inheritance
 Looking at the [code](https://github.com/BabylonJS/Babylon.js/tree/master/packages/dev/core/src/Cameras), we have a TargetCamera (inherits from Camera), from which other cameras inherit::

- TargetCamera
  - ArcRotateCamera
  - FlyCamera
  - FollowCamera
  - FreeCamera (legacy)
    - DeviceOrientationCamera
    - TouchCamera
      - **UniversalCamera**
        - GamepadCamera
    - VirtualJoysticksCamera

**Note:** I ignore RigModes, Stereoscopic cameras, and VR cameras. I also ignore the GamepadCamera (use niversalCamera instead)

## Descriptions from manual
- **TargetCamera** ([ref](https://doc.babylonjs.com/features/featuresDeepDive/cameras)): A target camera takes a mesh or position as a target and continues to look at it while it moves.  
  This is the base of the follow, arc rotate cameras and Free camera  
  - **ArcRotateCamera** ([ref](https://doc.babylonjs.com/features/featuresDeepDive/cameras/camera_introduction#arc-rotate-camera)):
  This represents an orbital type of camera.
  This camera always points towards a given target position and can be rotated around that target with the target as the centre of rotation. It can be controlled with cursors and mouse, or with touch events.
  Think of this camera as one **orbiting its target position**, or more imaginatively as a spy satellite orbiting the earth. Its position relative to the target (earth) can be set by three parameters, alpha (radians) the longitudinal rotation, beta (radians) the latitudinal rotation and radius the distance from the target position.  
  - **FlyCamera**: This is a flying camera, designed for 3D movement and rotation in all directions,  
such as in a 3D Space Shooter or a Flight Simulator.
  - **FollowCamera** ([ref](https://doc.babylonjs.com/features/featuresDeepDive/cameras/camera_introduction#followcamera)): A follow camera takes a mesh as a target and follows it as it moves. Both a free camera version followCamera and an arc rotate version arcFollowCamera are available.  
  - **FreeCamera (legacy)** ([ref](https://doc.babylonjs.com/features/featuresDeepDive/cameras/camera_introduction#universal-camera)): This represents a free type of camera. It can be useful in First Person Shooter game for instance. Please consider using the new UniversalCamera instead as it adds more functionality like the gamepad.  
    - **DeviceOrientationCamera:** This is a camera specifically designed to react to device orientation events such as a modern mobile device being tilted forward or back and left or right.
    - **TouchCamera** ([ref](https://doc.babylonjs.com/features/featuresDeepDive/cameras/camera_introduction#universal-camera)): This represents a FPS type of camera controlled by touch. This is like a universal camera minus the Gamepad controls.
      - **UniversalCamera** ([ref](https://doc.babylonjs.com/features/featuresDeepDive/cameras/camera_introduction#universal-camera)): The Universal Camera is the one to choose for first person shooter type games, and works with all the keyboard, mouse, touch and gamepads. This replaces the earlier Free Camera, which still works and will still be found in many Playgrounds.

## Examples
### FlyCamera
Controls:
- mouse wheel: engine increase/decrease
<center><canvas id='example1'></canvas></center>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
