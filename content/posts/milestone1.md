---
title: "Milestone 1: First steps in 3D"
subtitle: 
date: 2023-04-16
tags: [ "wip" ]
---
<script src="https://cdn.babylonjs.com/babylon.js"></script>
<script src="/media/milestone1/example1.js"></script>
<script src="/media/milestone1/example2.js"></script>
<script src="/media/milestone1/example3.js"></script>
<script src="/media/milestone1/example4.js"></script>
<script src="/media/milestone1/example5.js"></script>
<script src="/media/milestone1/example6.js"></script>
<style>
  canvas { width: 70%; },
  img { width: 70%; },
</style>

{{<toc>}}

## Milestone 1: First steps in 3D
From the initial read of the user guide I get the below.
<center><canvas id='example1'></canvas></center>

Now, we draw a triangle to represent a ship.
<center><canvas id='example2'></canvas></center>

### Figure of eight/cuban eight
animate ship: just flying around, no need to worry about physics

<center><img src="/media/milestone1/cuban-eight.gif"></img><br>
img source: <a href="https://www.rc-airplane-world.com/cuban-8-rc-airplane-aerobatics.html">https://www.rc-airplane-world.com/cuban-8-rc-airplane-aerobatics.html</a></center>

For the animation, we are going to animate the position and two rotations, four animations in total:
- position: figure eight:
  - x: sin
  - y: sin
- rotation1: up/down
- rotation2: roll


My first attempt at the animation looks like this.
<center><canvas id='example3'></canvas></center>

But once we add the roll rotation, it doesn't work as it should.
<center><canvas id='example4'></canvas></center>

This is because there are three static axes of rotation: x, y, z, and they stay static. When the ship turns up (pitch), its axis of rotation (roll) should change also. I think I can solve this with [this article](https://doc.babylonjs.com/features/featuresDeepDive/mesh/transforms/parent_pivot/pivot).

We try this here:
<center><canvas id='example5'></canvas></center>

It seems to work. Now, I'd like to use an actual smooth trajectory path for my figure of eight (Bezier curve?), instead of hand-coding the x,y positions.

### Smoother figure of eight
We consider:
- figure eight curve (lemniscate of Gerono):
  - [wiki: Lemniscate of Gerono](https://en.wikipedia.org/wiki/Lemniscate_of_Gerono)
  - [Eight curve](https://mathshistory.st-andrews.ac.uk/Curves/Eight/)
- [wiki: Lissajous curve](https://en.wikipedia.org/wiki/Lissajous_curve)
- cubic bezier curve?

Look at BabylonJS features:

- [path3D](https://doc.babylonjs.com/features/featuresDeepDive/mesh/path3D)
- [drawCurves](https://doc.babylonjs.com/features/featuresDeepDive/mesh/drawCurves)

<center><canvas id='example6'></canvas></center>

There is a small jump/glitch at one point, but this will do for now.
